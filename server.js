const express = require('express')
const app = express()
const port = 3000
const records = require('./data.json')

// console.log(JSON.stringify(records.data));

app.get('/records/:items_per_request/:last_index', (req, res) => {
    let data = records.data

    let temp = []
    let count =0;
    let totalCount = data.length;
    data.forEach(element => {
        if(element.id>=req.params.last_index){
            console.log(temp);
            temp.push(element)
            count++

            if(count==req.params.items_per_request){
              return  res.status(200).send({
                    data:temp,
                    totalCount
                })   
            }
        }
    });

})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})