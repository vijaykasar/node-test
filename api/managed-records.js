const fetch = require("node-fetch");

let Ids = [];
let Open = [];
let ClosedCount;
let PreviousPage;
let NextPage;

fetch(`http://localhost:3000/records/10/1`,{
    method: 'GET', // or 'PUT'
    headers: {
      'Content-Type': 'application/json',
    },
  }).then(response => response.json())
  .then(data => {
    console.log('Success:', data);
     Ids = [];
    Open = [];
    ClosedCount = 0
    data.data.forEach(element => {
        Ids.push(element.id);
        if(element.disposition=='open'){
            Open.push(element)
        }
        if(element.disposition=='closed' && element.color=='red'||'green'||'blue'){
            ClosedCount  = ClosedCount+1
        }
        
    });

  })
  .catch((error) => {
    console.error('Error:', error);
  });